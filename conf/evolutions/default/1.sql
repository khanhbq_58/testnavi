# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table stock_item (
  id                        bigint not null,
  warehouse_id              bigint,
  student_id                bigint,
  quantity                  bigint,
  constraint pk_stock_item primary key (id))
;

create table student (
  id                        bigint not null,
  stt                       integer,
  ten                       varchar(255),
  ngaysinh                  timestamp,
  mssv                      varchar(255),
  diachi                    varchar(255),
  email                     varchar(255),
  pass                      varchar(255),
  nganh                     varchar(255),
  picture                   varbinary(255),
  constraint pk_student primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  kiemtra                   varchar(255),
  constraint pk_user_account primary key (id))
;

create table warehouse (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_warehouse primary key (id))
;

create sequence stock_item_seq;

create sequence student_seq;

create sequence user_account_seq;

create sequence warehouse_seq;

alter table stock_item add constraint fk_stock_item_warehouse_1 foreign key (warehouse_id) references warehouse (id) on delete restrict on update restrict;
create index ix_stock_item_warehouse_1 on stock_item (warehouse_id);
alter table stock_item add constraint fk_stock_item_student_2 foreign key (student_id) references student (id) on delete restrict on update restrict;
create index ix_stock_item_student_2 on stock_item (student_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists stock_item;

drop table if exists student;

drop table if exists user_account;

drop table if exists warehouse;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists stock_item_seq;

drop sequence if exists student_seq;

drop sequence if exists user_account_seq;

drop sequence if exists warehouse_seq;

