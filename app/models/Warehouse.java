package models;
import java.util.*;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;
@Entity
public class Warehouse extends Model {
    @Id
    public Long id;

    public String name;
    @OneToMany(mappedBy="warehouse")
    public List<StockItem> stock = new ArrayList();  // trường quan hệ
    public String toString() {
        return name;
    }
    public static Finder<Long, Warehouse> find =
            new Finder<>(Long.class, Warehouse.class);
    public static Warehouse findById(Long id) {
        return find.byId(id);
    }
}