package models;
import play.data.validation.Constraints;
import play.mvc.PathBindable;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;
import play.*;
import com.avaje.ebean.Page;
import play.data.format.Formats;

@Entity
public class Student extends Model implements PathBindable<Student>{
    @Id
    public Long id;
    public int stt ;
    @Constraints.Required
    public String ten;
    @Constraints.Required
    @Formats.DateTime(pattern = "dd-MM-yyyy")
    public Date ngaysinh;
    @Constraints.Required
    public String mssv;
    @Constraints.Required
    public String diachi;
    @Constraints.Required
    public String email;
    public String pass;
    @Constraints.Required
    public String nganh;
    public byte[] picture;
    @OneToMany(mappedBy="student")
    public List<StockItem> stockItems;
    public static ArrayList<String> options() {
        ArrayList<String> options = new ArrayList<String>();
        options.add("Công nghệ Cơ điện tử");
        options.add("Công nghệ Thông tin");
        options.add("Công nghệ Điện tử - Viễn thông");
        options.add("Truyền thông và Mạng máy tính");
        options.add("Công nghệ Kỹ thuật Cơ điện tử");
        options.add("Cơ học Kỹ thuật");
        options.add("Vật lý Kỹ thuật");
        options.add("Khoa học Máy tính");
        options.add("Hệ thống Thông tin");
        return options;

    }
    public Student() {}
    public Student(int stt, String ten, Date ngaysinh, String mssv, String diachi, String email,String nganh)
    {
        this.pass=mssv;
        this.ten = ten;
        this.ngaysinh = ngaysinh;
        this.mssv = mssv;
        this.diachi = diachi;
        this.email = email;
        this.nganh = nganh;
    }

    private static List<Student> students;

    public static Finder<Long,Student> find = new Finder<Long,Student>(Long.class, Student.class);

    public static List<Student> findAll() {
        return find.all();
    }
    public static Page<Student> find(int page) {  // trả về trang thay vì List
            return find.where()
                    .orderBy("id asc")     // sắp xếp tăng dần theo id
                    .findPagingList(10)    // quy định kích thước của trang
                    .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                    .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
    public static Student findByEmail(String email) {
        return find.where().eq("Email", email).findUnique();
    }
    public static Student findByMSSV(String mssv) {
        return find.where().eq("MSSV", mssv).findUnique();
    }
    public static Student findByName(String term){
        return find.where().eq("name", term).findUnique();
    }
    public static Student authenticate(String email,String password){
        return find.where().eq("email", email).eq("pass", password).findUnique();
    }
    public static boolean remove(Student student) {
        return students.remove(student);
    }
    @Override
    public Student bind(String key, String value) {
        return findByMSSV(value);
    }
    @Override
    public String unbind(String key) {
        return mssv;
    }

    @Override
    public String javascriptUnbind() {
        return mssv;
    }

    public String toString() {
        return String.format("  %s -%s- $s", ten, mssv,ngaysinh);
    }
}
