package models;
import java.util.*;
import play.mvc.*;
import play.data.validation.Constraints;
import com.avaje.ebean.Page;
public class Teacher implements PathBindable<Teacher> {
    @Constraints.Required
    public String ten;
    @Constraints.Required
    public String chucvu;
    public String sodienthoai;
    public Teacher() {}
    public Teacher(String ten, String chucvu, String sodienthoai) {
        this.ten = ten;
        this.chucvu = chucvu;
        this.sodienthoai = sodienthoai;
    }
    public String toString() {
        return String.format("%s - %s - %s", ten, chucvu,sodienthoai);
    }
    private static List<Teacher> teachers;
    static {
        teachers = new ArrayList<Teacher>();
        teachers.add(new Teacher("Phạm Vân An", "Giáo Sư",
                "09xxxxxxx1"));
        teachers.add(new Teacher("Ngô Bảo Châu", "Tiến Sĩ",
                "09xxxxxxx2"));
        teachers.add(new Teacher("Nguyễn Văn Hiếu", "Thạc Sĩ",
                "09xxxxxxx3"));
        teachers.add(new Teacher("Trần Văn Hùng", "Giáo Sư",
                "09xxxxxxx4"));
        teachers.add(new Teacher("Ngô Thi Hằng", "Tiến Sĩ",
                "09xxxxxxx5"));
    }
    public static List<Teacher> findAll() {
        return new ArrayList<Teacher>(teachers);
    }
    public static Teacher findByTen(String ten) {
        for (Teacher candidate : teachers) {
            if (candidate.ten.equals(ten)) {
                return candidate;
            }
        }
        return null;
    }
    public static boolean remove(Teacher teacher) {
        return teachers.remove(teacher);
    }
    public void save() {
        teachers.remove(findByTen(this.ten));
        teachers.add(this);
    }

    @Override
    public Teacher bind(String key, String value) {
        return findByTen(value);
    }
    @Override
    public String unbind(String key) {
        return ten;
    }
    @Override
    public String javascriptUnbind() {
        return ten;
    }
}