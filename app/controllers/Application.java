package controllers;

import play.*;
import play.mvc.*;
import static play.data.Form.form;
import models.UserAccount;
import models.Student;
import models.Teacher;
import play.data.Form;
import views.html.*;

public class Application extends Controller {
    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
   public static class Login {
        public String email;
        public String password;
    }
    public static Result login() {
        return ok(
                login.render(form(Login.class))
        );
    }
    public static Result logout() {
        session().clear();
        return redirect(routes.Application.login());
    }
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
        session().clear();
        if ((UserAccount.authenticate(email, password) == null) && (Student.authenticate(email,password)==null)){
            flash("error", "email hoặc mật khẩu không đúng");
            return redirect(routes.Application.login());
        }

        session("email", email);
        if ((UserAccount.authenticate(email, password) != null) && (Student.authenticate(email,password)==null)) {
            return redirect(routes.Students.list(0));
        }
        return redirect(routes.Teachers.list());
    }
}
