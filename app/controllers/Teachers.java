package controllers;

import models.*;
import play.mvc.*;
import play.data.Form;
import java.util.*;
import views.html.*;
import views.html.teachers.*;

public class Teachers extends Controller
{
    private static final Form<Teacher> teacherForm = Form.form(Teacher.class);

    public static Result list() {
        List<Teacher> teachers = Teacher.findAll();
        return ok(list.render(teachers));
    }

    public static Result newTeacher() {
        return ok(details.render(teacherForm));
    }

    public static Result details(Teacher teacher) {
        if (teacher == null) {
            return notFound(String.format("Teacher %s does not exist.", teacher.ten));
        }
        Form<Teacher> filledForm = teacherForm.fill(teacher);
        return ok(details.render(filledForm));
    }
    public static Result delete(String ten) {
        final Teacher teacher = Teacher.findByTen(ten);
        if(teacher == null) {
            return notFound(String.format("Teacher %s does not exists.",ten));
        }
        Teacher.remove(teacher);
        return redirect(routes.Teachers.list());
    }
    public static Result save() {
        Form<Teacher> boundForm = teacherForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Teacher teacher = boundForm.get();
        teacher.save();
        flash("success", String.format("Successfully added teacher %s", teacher));
        return redirect(routes.Teachers.list());
    }
}
